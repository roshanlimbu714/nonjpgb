from django.db import models
from datetime import datetime
# Create your models here.
class Realtor(models.Model):
	name=models.CharField(max_length=255)
	address=models.CharField(max_length=255)
	description=models.TextField(blank=True)
	is_active=models.BooleanField(default=False)
	phone=models.CharField(max_length=10)
	photo=models.ImageField(upload_to='%Y/%m/%d',blank=True)
	reg_date=models.DateTimeField(default=datetime.now)
	def __str__(self):
		return self.name