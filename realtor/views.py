from django.shortcuts import render
from .models import Realtor
# Create your views here.
def listRealtors(request):
	realtors=Realtor.objects.all()
	context={
		'realtors':realtors
	}
	return render(request,'realtor/realtors.html',context)


def viewRealtor(request,realtorid):
	realtor=Realtor.objects.get(id=realtorid)
	context={
		'realtors':realtor
	}
	return render(request,'realtor/realtor.html',context)
