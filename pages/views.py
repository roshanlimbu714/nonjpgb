from django.shortcuts import render
from listings.models import Listing
# Create your views here.
def index(request):
	popularLists=Listing.objects.all()[:4]
	latestLists=Listing.objects.all().order_by('id')[:6]
	context={
	'title':'Ghar Aagan',
	'popularLists':popularLists,
	'latestLists':latestLists
	}
	return render(request,'pages/index.html',context)